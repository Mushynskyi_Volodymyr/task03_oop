package com.epam.controller;

public interface Controller {
    void printProductList();
    void printAllInfoProduct(int key);
    void searchProductForTree();
    void searchProductForFlat();
    void buyProduct(int key);
    void viewBasket();
}
