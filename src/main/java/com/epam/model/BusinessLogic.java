package com.epam.model;

public class BusinessLogic implements Model {
    Domain domain;

    public BusinessLogic() {domain = new Domain();}
    @Override
    public void printProductList() {
        domain.printProductList();
    }

    @Override
    public void printAllInfoProduct(int key) {
        domain.printAllInfoProduct(key);
    }

    @Override
    public void searchProductForTree() {
        domain.searchProductForTree();
    }

    @Override
    public void searchProductForFlat() {
        domain.searchProductForFlat();
    }

    @Override
    public void buyProduct(int key) {
        domain.buyProduct(key);
    }

    @Override
    public void viewBasket() {
        domain.viewBasket();
    }
}

