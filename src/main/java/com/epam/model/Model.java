package com.epam.model;

public interface Model {
    void printProductList();
    void printAllInfoProduct(int key);
    void searchProductForTree();
    void searchProductForFlat();
    void buyProduct(int key);
    void viewBasket();
}

