package com.epam.model.project;

public class Decoration {
    private String name;
    private boolean avaibility;
    private int price;
    private int order;
    private String purpose;
    private int amountOfProduct;


    public Decoration(String name, boolean avaibility, int price) {
        this.name = name;
        this.avaibility = avaibility;
        this.price = price;
        }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isAvaibility() {
        return avaibility;
    }

    public void setAvaibility(boolean avaibility) {
        this.avaibility = avaibility;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }


    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public int getAmountOfProduct() {
        return amountOfProduct;
    }

    public void setAmountOfProduct() {
        this.amountOfProduct = amountOfProduct;
    }

    @Override
    public String toString() {
        return "Decoration{" +
                "name='" + name + '\'' +
                ", avaibility=" + avaibility +
                ", price=" + price;
    }
}



