package com.epam.model.project;

public class BallsForTree extends Decoration {
    private String color;
    private String size;

    public BallsForTree(String name, boolean avaibility, int price, String color, String size)
    {
        super(name, avaibility, price);
        this.color = color;
        this.size = size;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    @Override
    public String toString() {
        return "BallsForTree"+ super.toString() +
                " color='" + color + '\'' +
                ", size='" + size + '\'' +
                '}';
    }
}
