package com.epam.model.project;

import com.epam.model.project.Decoration;

public class Garland extends Decoration {
    private int lampNumbers;
    private int colorNumbers;
    private String description;
    private int lenght;

    public Garland(String name, boolean avaibility, int price, int lampNumbers, int colorNumbers, String description, int lenght)
    {
        super(name, avaibility, price);
        this.lampNumbers = lampNumbers;
        this.colorNumbers = colorNumbers;
        this.description = description;
        this.lenght = lenght;
    }

    public int getLampNumbers() {
        return lampNumbers;
    }

    public void setLampNumbers(int lampNumbers) {
        this.lampNumbers = lampNumbers;
    }

    public int getColorNumbers() {
        return colorNumbers;
    }

    public void setColorNumbers(int colorNumbers) {
        this.colorNumbers = colorNumbers;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getLenght() {
        return lenght;
    }

    public void setLenght(int lenght) {
        this.lenght = lenght;
    }

    @Override
    public String toString() {
        return "Garland" + super.toString() +
                " lampNumbers=" + lampNumbers +
                ", colorNumbers=" + colorNumbers +
                ", description='" + description + '\'' +
                ", lenght=" + lenght +
                '}';
    }
}
