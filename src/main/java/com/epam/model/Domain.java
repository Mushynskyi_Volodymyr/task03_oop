package com.epam.model;

import com.epam.model.project.BallsForTree;
import com.epam.model.project.Decoration;
import com.epam.model.project.Garland;

import java.util.LinkedList;
import java.util.List;

public class Domain {
    private final int CONST1 = 1;
    private List<Object> objectList;
    private List<Object> buyList;
    private int totalSum = 0;

    Domain() {
        objectList = new LinkedList<>();
        buyList = new LinkedList<>();
        Decoration ball1 = new BallsForTree("Ball type 1", true, 60, "Red", "Big");
        Decoration ball2 = new BallsForTree("Ball type 2", false, 10, "Green", "Small");
        Decoration ball3 = new BallsForTree("Ball type 3", true, 110, "Blue", "Medium");
        Decoration garland1 = new Garland("Garland type 1", true, 100, 125, 4, "For flat", 5);
        Decoration garland2 = new Garland("Garland type 2", false, 140, 150, 6, "For street", 7 );
        Decoration garland3 = new Garland("Garland type 3", false, 160, 150, 5, "For street", 10);

        objectList.add(garland1);
        objectList.add(garland2);
        objectList.add(garland3);
        objectList.add(ball1);
        objectList.add(ball2);
        objectList.add(ball3);
    }

    public void printProductList() {
        printShortInformation((Decoration) objectList.get(0));
        printShortInformation((Decoration) objectList.get(1));
        printShortInformation((Decoration) objectList.get(2));
        printShortInformation((Decoration) objectList.get(3));
        printShortInformation((Decoration) objectList.get(4));
        System.out.println("Please, select number");
    }

    private void printShortInformation(Decoration decoration) {
        System.out.println("===============================");
        System.out.println("Name: " + decoration.getName());
        System.out.println("Price: " + decoration.getPrice());
        System.out.println("SELECT: " + decoration.getOrder());
        System.out.println("===============================");
    }

    public void printAllInfoProduct(int key) {
        System.out.println(objectList.get(key));
    }

    public void searchProductForFlat() {
        for (int i = 0; i < objectList.size(); i++) {
            if (forSearch((Decoration) objectList.get(i)).equals("For flat")) {
                printShortInformation((Decoration) objectList.get(i));
            }
        }
    }

    public void searchProductForTree() {
        for (int i = 0; i < objectList.size(); i++) {
            if (forSearch((Decoration) objectList.get(i)).equals("For tree")) {
                printShortInformation((Decoration) objectList.get(i));
            }
        }
    }

    private String forSearch(Decoration decoration) {
        return decoration.getPurpose();
    }

    public void buyProduct(int key) {
        buyList.add(objectList.get(key - CONST1));
        System.out.println("Enter amount of product: ");
        ((Decoration)objectList.get(key - CONST1)).setAmountOfProduct();
        System.out.println("This product added in basket");
    }

    private void productInBasket(Decoration decoration) {
        System.out.println("\nYour basket: ");
        System.out.println("===============================");
        System.out.println("Name: " + decoration.getName());
        System.out.println("Price: " + decoration.getPrice());
        System.out.println("Amount: " + decoration.getAmountOfProduct());
        System.out.println("Total: " + (decoration.getPrice() * decoration.getAmountOfProduct()));
        totalSum += decoration.getPrice() * decoration.getAmountOfProduct();
        System.out.println("===============================");
    }

    public void viewBasket() {
        for (int i = 0; i < buyList.size(); i++) {
            productInBasket((Decoration) buyList.get(i));
        }
        System.out.println("Total price: " + totalSum);
        System.out.println("Q - Back into main menu");
    }
}

